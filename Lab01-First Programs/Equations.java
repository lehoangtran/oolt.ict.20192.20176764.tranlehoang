import javax.swing.JOptionPane;

public class Equations {

    private void secondDegree(double a, double b, double c) {
        double delta = b*b - 4*a*c;
        if (delta == 0) {

        } 
        else if (delta > 0) {

        }
        else {
            
        }
    }

    public static void main(String[] args) {
    	 String strNum1 = JOptionPane.showInputDialog(null, "Please input the first number: ", "First Degree equation", JOptionPane.INFORMATION_MESSAGE);
         String strNum2 = JOptionPane.showInputDialog(null, "Please input the second number: ", "First Degree equation", JOptionPane.INFORMATION_MESSAGE);
         double a = Double.parseDouble(strNum1);
         double b = Double.parseDouble(strNum2);
         if (a == 0) {
             JOptionPane.showMessageDialog(null, "No result", "First Degree equation", JOptionPane.INFORMATION_MESSAGE);
         } else {
        	 String message = String.format("The equation has 1 solution: %.2f", -b/a);
        	 JOptionPane.showMessageDialog(null, message, "First Degree equation", JOptionPane.INFORMATION_MESSAGE);
         }
         
         // -----------------------------------------------------------------
         
         String strNuma11 = JOptionPane.showInputDialog(null, "Please input a11: ", "Linear equation", JOptionPane.INFORMATION_MESSAGE);
         String strNuma12 = JOptionPane.showInputDialog(null, "Please input a12: ", "Linear equation", JOptionPane.INFORMATION_MESSAGE);
         String strNumb1 = JOptionPane.showInputDialog(null, "Please input b1: ", "Linear equation", JOptionPane.INFORMATION_MESSAGE);
         String strNuma21 = JOptionPane.showInputDialog(null, "Please input a21: ", "Linear equation", JOptionPane.INFORMATION_MESSAGE);
         String strNuma22 = JOptionPane.showInputDialog(null, "Please input a22: ", "Linear equation", JOptionPane.INFORMATION_MESSAGE);
         String strNumb2 = JOptionPane.showInputDialog(null, "Please input b2: ", "Linear equation", JOptionPane.INFORMATION_MESSAGE);
         double a11 = Double.parseDouble(strNuma11);
         double a12 = Double.parseDouble(strNuma12);
         double b1 = Double.parseDouble(strNumb1);
         double a21 = Double.parseDouble(strNuma21);
         double a22 = Double.parseDouble(strNuma22);
         double b2 = Double.parseDouble(strNumb2);
         double D = a11*a22 - a21*a12;
         double D1 = b1*a22 - b2*a12;
         double D2 = a11*b2 - a21*b1;

         if (D != 0) {
        	 double r1 = D1 / D;
        	 double r2 = D2 / D;
        	 String message = String.format("The equation has 2 solution: %.2f and %.2f", r1, r2);
        	 JOptionPane.showMessageDialog(null, message, "First Degree equation", JOptionPane.INFORMATION_MESSAGE);
         }	
         else if (D1 == 0 && D2 == 0 && D == D1) {
        	 JOptionPane.showMessageDialog(null, "Infinite Solutution", "Linear equation", JOptionPane.INFORMATION_MESSAGE);
         }
         else {
        	 JOptionPane.showMessageDialog(null, "No solution", "Linear equation", JOptionPane.INFORMATION_MESSAGE);
         }
         
         
    }

}