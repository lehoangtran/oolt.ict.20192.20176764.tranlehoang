import java.util.Scanner;

public class DateUtil {

	public static boolean isLeapYear(int year) {
		if (year % 100 == 0 && year % 400 != 0 || year % 4 != 0)
			return false;
		return true;
	}

	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);

		System.out.print("Enter a month: ");
		String inputMonth = scanner.nextLine();

		int inputYear;
		do {
			System.out.println("Enter a year: ");
			inputYear = Integer.parseInt(scanner.nextLine());
			if (inputYear < 0)
				System.out.println("Not Valid. Re-input!");
		} while (inputYear < 0);

		switch (inputMonth) {
		case "January", "Jan.", "1":
			System.out.println(31);
			break;
		case "Febuary", "Feb.", "2":
			if (isLeapYear(inputYear)) {
				System.out.println(29);
			} else
				System.out.println(28);
			break;
		case "March", "Mar.", "3":
			System.out.println(31);
			break;
		case "April", "Apr.", "4":
			System.out.println(30);
			break;
		case "May", "5":
			System.out.println(31);
			break;
		case "June", "Jun.", "6":
			System.out.println(30);
			break;
		case "July", "Jul.", "7":
			System.out.println(31);
			break;
		case "August", "Aug.", "8":
			System.out.println(31);
			break;
		case "September", "Sep.", "9":
			System.out.println(30);
			break;
		case "October", "Oct.", "10":
			System.out.println(31);
			break;
		case "November", "Nov.", "11":
			System.out.println(30);
			break;
		case "December", "Dec.", "12":
			System.out.println(31);
			break;
		default:
			System.out.println("Unexpected value: " + inputMonth);
			break;
		}
		scanner.close();
	}
}
