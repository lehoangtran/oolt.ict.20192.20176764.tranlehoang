package main;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.Date;
import java.util.Scanner;

public class MyDate {
	private int day;
	private int month;
	private int year;
	
	private String getDaySuffix(int day) {
		if (day >= 11 && day <= 13) {
			return "th";
		}
		switch (day % 10) {
		case 1:
			return "st";
		case 2:
			return "nd";
		case 3:
			return "rd";
		default:
			return "th";
		}
	}
	
	//Convert String to LocalDate (including check date format) 
	private LocalDate convertToLocalDate(String dateStr) {
		int index[] = new int[4];
		if (dateStr.contains("th") || dateStr.contains("st") || dateStr.contains("rd") || dateStr.contains("nd")) {
			index[0] = dateStr.indexOf("th");
			index[1] = dateStr.indexOf("st");
			index[2] = dateStr.indexOf("nd");
			index[3] = dateStr.indexOf("rd");
			for (int i = 0; i < index.length; i++) {
				if (index[i] > 0) {
					int day = Integer.parseInt(dateStr.substring(index[i] - 2, index[i]).trim());
					if (i == 0) {
						String temp = getDaySuffix(day);
						if (temp != "th")
							return null;
						dateStr = dateStr.replace("th", "");
					} else if (i == 1) {
						String temp = getDaySuffix(day);
						if (temp != "st")
							return null;
						dateStr = dateStr.replace("st", "");
					} else if (i == 2) {
						String temp = getDaySuffix(day);
						if (temp != "nd")
							return null;
						dateStr = dateStr.replace("nd", "");
					} else if (i == 3) {
						String temp = getDaySuffix(day);
						if (temp != "rd")
							return null;
						dateStr = dateStr.replace("rd", "");
					}
					break;
				}
			}
		}

		SimpleDateFormat simpleDateFormat = new SimpleDateFormat("MMMM dd yyyy");
		Date date = new Date();
		try {
			date = simpleDateFormat.parse(dateStr);
			return date.toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
		} catch (ParseException e) {
			return null;
		}
	}

	public MyDate() {

	}

	public MyDate(String dateStr) {
		LocalDate localDate = convertToLocalDate(dateStr);
		if (localDate == null) {
			System.out.println("Input Date Error");
			return;
		}

		System.out.println(localDate.toString());
		int year = localDate.getYear();
		int month = localDate.getMonthValue();
		int day = localDate.getDayOfMonth();
		this.setDay(day);
		this.setMonth(month);
		this.setYear(year);
	}

	public MyDate(int day, int month, int year) {
		this.day = day;
		this.month = month;
		this.year = year;
	}

	public int getDay() {
		return day;
	}

	public void setDay(int day) {
		this.day = day;
	}

	public int getMonth() {
		return month;
	}

	public void setMonth(int month) {
		this.month = month;
	}

	public int getYear() {
		return year;
	}

	public void setYear(int year) {
		this.year = year;
	}

	public void accept() {
		Scanner scan = new Scanner(System.in);
		System.out.print("Input a date: ");
		String dateStr = scan.nextLine();
		LocalDate localDate = convertToLocalDate(dateStr);
		if (localDate == null) {
			System.out.println("Input Date Error");
			return;
		}

		System.out.println(localDate.toString());
		int year = localDate.getYear();
		int month = localDate.getMonthValue();
		int day = localDate.getDayOfMonth();
		this.setDay(day);
		this.setMonth(month);
		this.setYear(year);
	}

	public void print() {
		System.out.format("Day: %d\nMonth: %d\nYear: %d\n", this.getDay(), this.getMonth(), this.getYear());
	}

}
