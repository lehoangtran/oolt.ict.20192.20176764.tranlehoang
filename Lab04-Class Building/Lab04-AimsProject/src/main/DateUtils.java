package main;

import java.time.LocalDate;
import java.util.List;
import java.util.stream.Collectors;

public class DateUtils {
	public static void dateCompare(LocalDate d1, LocalDate d2) {
		if (d1.compareTo(d2) > 0) 
			System.out.println(d1.toString() + " is later than " + d2.toString());
		else 
			System.out.println(d1.toString() + " is earlier than " + d2.toString());
	}
	
	public static List<LocalDate> sortDates(List<LocalDate> list) {
		return list.stream().sorted(LocalDate::compareTo).collect(Collectors.toList());
	}
}
