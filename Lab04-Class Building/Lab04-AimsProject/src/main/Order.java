package main;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

public class Order {
	public static final int MAX_NUMBER_ORDERED = 10;
	public static final int MAX_LIMITED_ORDERS = 3;
	private static int nbOrders = 0;

	private List<DigitalVideoDisc> itemsOrdered = new ArrayList<>();
	private int qtyOrdered;
	private Date dateOrdered;

	// ---------getter setter constructor-------------------------

	public Date getDateOrdered() {
		return dateOrdered;
	}

	public void setDateOrdered(Date dateOrdered) {
		this.dateOrdered = dateOrdered;
	}

	public int getQtyOrdered() {
		return qtyOrdered;
	}

	public void setQtyOrdered(int qtyOrdered) {
		this.qtyOrdered = qtyOrdered;
	}

	public Order() {
		if (nbOrders + 1 <= MAX_LIMITED_ORDERS) {
			this.dateOrdered = new Date();
			nbOrders++;
			System.out.println("Order " + nbOrders + " initialized");
		} else {
			System.out.println("Max orders reached");
			System.exit(0);
		}
	}

	// ----------service------------------------

	public void addDigitalVideoDisc(DigitalVideoDisc disc) {
		if (qtyOrdered < MAX_NUMBER_ORDERED) {
			itemsOrdered.add(disc);
			qtyOrdered = itemsOrdered.size();
		} else {
			System.out.println("The order is full");
		}
	}

	// Overloading (add a list)
	public void addDigitalVideoDisc(List<DigitalVideoDisc> dvdList) {
		if (qtyOrdered + dvdList.size() < MAX_NUMBER_ORDERED) {
			itemsOrdered.addAll(dvdList);
			qtyOrdered = itemsOrdered.size();
		} else {
			System.out.println("The order is full...");
		}
	}

	// Overloading (add 2 disks)
	public void addDigitalVideoDisc(DigitalVideoDisc dvd1, DigitalVideoDisc dvd2) {
		if (qtyOrdered + 2 < MAX_NUMBER_ORDERED) {
			itemsOrdered.add(dvd1);
			itemsOrdered.add(dvd2);
			qtyOrdered = itemsOrdered.size();
		} else {
			System.out.println("The order is full");
		}
	}

	public void removeDigitalVideoDisc(DigitalVideoDisc disc) {
		if (qtyOrdered != 0) {
			itemsOrdered = itemsOrdered.stream().filter(d -> d != disc).collect(Collectors.toList());
			qtyOrdered = itemsOrdered.size();
			System.out.format("The disc \"%s\" has been removed\n", disc.getTitle());
		} else {
			System.out.println("Order is empty. Please add!");
		}
	}

	public double totalCost() {
		return itemsOrdered.stream().mapToDouble(d -> d.getCost()).sum();
	}

	public void printOrder() {
		System.out.println("************************* ORDER ************************");
		System.out.println("Date: " + getDateOrdered());
		System.out.println("Ordered items: ");
		for (int i = 0; i < itemsOrdered.size(); i++) {
			System.out.println((i + 1) + ". " + itemsOrdered.get(i).getTitle() + " - "
					+ itemsOrdered.get(i).getCategory() + " - " + itemsOrdered.get(i).getDirectory() + " - "
					+ itemsOrdered.get(i).getLength() + ": $" + itemsOrdered.get(i).getCost());
		}
		System.out.format("\nTotal Cost is: %.2f$\n", totalCost());
		System.out.println("******************************************************\n");
	}

}
