package hust.soict.globalict.aims;

import java.util.ArrayList;
import java.util.List;

import hust.soict.globalict.aims.disc.DigitalVideoDisc;
import hust.soict.globalict.aims.order.Order;

public class Aims {
	public static void main(String[] args) {
		// ORDER 1
		Order order1 = new Order();
		order1.addDigitalVideoDisc(new DigitalVideoDisc("The Lion King", "Animation", "Roger Allers", 86, 19.95f));
		order1.addDigitalVideoDisc(new DigitalVideoDisc("Star Wars", "Science Fiction", "George Lucas", 124, 24.95f));
		order1.addDigitalVideoDisc(new DigitalVideoDisc("Alladin", "Animation", "John Musker", 90, 18.99f));
		order1.printOrder();

		// ORDER 2
		Order order2 = new Order();
		order2.addDigitalVideoDisc(new DigitalVideoDisc("The Lion King", "Animation", "Roger Allers", 86, 19.95f),
				new DigitalVideoDisc("Star Wars", "Science Fiction", "George Lucas", 124, 24.95f));
		order2.printOrder();

		// ORDER 3
		Order order3 = new Order();
		List<DigitalVideoDisc> orderList = new ArrayList<>();
		orderList.add(new DigitalVideoDisc("The Lion Kingz", "Animation", "Roger Allers", 86, 29.95f));
		orderList.add(new DigitalVideoDisc("Star Warsz", "Science Fiction", "George Lucas", 124, 21.95f));
		orderList.add(new DigitalVideoDisc("Alladinz", "Animation", "John Musker", 90, 98.99f));
		order3.addDigitalVideoDisc(orderList);
		order3.printOrder();

		// ORDER 4
		Order order4 = new Order();
		order4.addDigitalVideoDisc(new DigitalVideoDisc("The Lion King", "Animation", "Roger Allers", 86, 19.95f),
				new DigitalVideoDisc("Star Wars", "Science Fiction", "George Lucas", 124, 24.95f));
		order4.printOrder();

	}
}
