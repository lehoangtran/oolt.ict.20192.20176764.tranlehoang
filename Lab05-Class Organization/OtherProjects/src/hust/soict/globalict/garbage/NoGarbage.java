package hust.soict.globalict.garbage;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;

public class NoGarbage {
	public static void main(String[] args) throws IOException {
		File f = new File("C:/Users/Dell/eclipse-workspace/OtherProjects/src/hust/soict/globalict/garbage/text.txt");
		if (!f.exists() && !f.canRead()) {
			System.out.println("Cant read file");
			return;
		}

		BufferedReader br = new BufferedReader(new FileReader(f));
		StringBuilder sb = new StringBuilder();
		int prevAddress = sb.hashCode();
		String line;
		int count = 0;
		
		System.out.println(prevAddress);
		
		while ((line = br.readLine()) != null) {
			sb.append(line);
			sb.append("\n");
			
			int curAddress = sb.hashCode();
			if (prevAddress != curAddress) {
				prevAddress = curAddress;
				System.out.println(curAddress);
				count += 1;
			}
		}
		System.out.println("Garbage: " + count);
		br.close();
	}
}
