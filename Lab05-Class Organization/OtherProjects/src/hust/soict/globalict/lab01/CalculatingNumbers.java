package hust.soict.globalict.lab01;
import javax.swing.JOptionPane;

public class CalculatingNumbers {
    public static void main(String[] args) {
        // ------------------------------------------------------------------
        String strNum1 = JOptionPane.showInputDialog(null, "Please input the first number: ", "Input the first number", JOptionPane.INFORMATION_MESSAGE);
        String strNum2 = JOptionPane.showInputDialog(null, "Please input the second number: ", "Input the first number", JOptionPane.INFORMATION_MESSAGE);

        double num1 = Double.parseDouble(strNum1);
        double num2 = Double.parseDouble(strNum2);

        double sum = num1 + num2;
        double dif = Math.abs(num1 - num2);
        double product = num1 * num2;
        double quotient = num1 / num2;

        String strNotification = String.format("Results between %.3f and %.3f:\nSum: %.3f\nDifference: %.3f\nProduct: %.3f\nQuotient: %.3f", num1, num2, sum, dif, product, quotient);
        JOptionPane.showMessageDialog(null, strNotification, "Show calculating results", JOptionPane.INFORMATION_MESSAGE);


        System.exit(0);
    }
}