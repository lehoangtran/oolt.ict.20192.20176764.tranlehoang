package hust.soict.globalict.aims;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.Scanner;
import java.util.stream.Collectors;

import hust.soict.globalict.aims.media.Book;
import hust.soict.globalict.aims.media.DigitalVideoDisc;
import hust.soict.globalict.aims.media.Media;
import hust.soict.globalict.aims.order.Order;

public class Aims {
	private static List<Media> storeList = new ArrayList<>();

	public static void showItems() {
		storeList.add(new DigitalVideoDisc("The Lion King", "Animation", "Roger Allers", 86, 29.95f));
		storeList.add(new DigitalVideoDisc("Star Wars", "Science Fiction", "George Lucas", 124, 21.95f));
		storeList.add(new DigitalVideoDisc("Alladin", "Animation", "John Musker", 90, 98.99f));
		storeList.add(new DigitalVideoDisc("Harry but not Potter", "Magic", "Hello World", 150, 49.99f));
		storeList.add(new Book("The Lion King 2", "Animation", Arrays.asList("Author a", "Author b"), 51.5f));
		storeList.add(new Book("Star Wars 2", "Science Fiction", Arrays.asList("Author c", "Author b"), 39.5f));
		storeList.add(new Book("Alladin 2", "Animation", Arrays.asList("Author d", "Author p"), 23.0f));
		storeList.add(new Book("Harry Potter", "Magic", Arrays.asList("Author e", "Author s"), 18.5f));
		storeList.add(new Book("The Lion King 3", "Animation", Arrays.asList("Author f", "Author z"), 102.2f));
		storeList.forEach(System.out::println);
	}

	public static void showMenu() {
		System.out.println("------------ All items in stock --------------");
		showItems();

		System.out.println("\nOrder Management Application: ");
		System.out.println("--------------------------------");
		System.out.println("1. Create new order (Max: 3 orders)");
		System.out.println("2. Add item to the order (Max: 10 items)");
		System.out.println("3. Delete item by id");
		System.out.println("4. Display the items list of order");
		System.out.println("0. Exit");
		System.out.println("--------------------------------");
		System.out.println("Please choose a number: 0-1-2-3-4");
		System.out.println("**HOT** Buy 5 get 1 random item for free of charge");
	}

	public static void main(String[] args) {
		int choice;
		int id = 0;
		Order order = null;
		Scanner scanner = new Scanner(System.in);

		showMenu();
		do {
			System.out.print("\nInput a choice: ");
			choice = Integer.parseInt(scanner.nextLine());

			switch (choice) {
			case 1:
				order = new Order();
				id = 0;
				break;

			case 2:
				if (order == null) {
					System.out.println("No order created");
					break;
				}

				String type;
				boolean check;

				System.out.println("You want to order Book or Digital Video Disc? ");
				System.out.println("Enter \"book\" or \"disc\" only");
				do {
					System.out.print("your choice: ");
					type = scanner.nextLine();
					check = type.toLowerCase().equals("book") || type.toLowerCase().equals("disc");
					if (!check)
						System.out.println("Not valid! Enter again");
				} while (!check);

				System.out.print("Enter title: ");
				String title = scanner.nextLine();

				if (type.toLowerCase().equals("book")) {
					Optional<Media> book = storeList.stream().filter(
							item -> item instanceof Book && item.getTitle().toLowerCase().equals(title.toLowerCase()))
							.findFirst();

					if (book.isPresent()) {
						id++;
						Book bookTemp = (Book) book.get();
						bookTemp.setId(id);
						order.addMedia(bookTemp);
						System.out.println("Book " + book.get().getTitle() +" added");
					} else
						System.out.println("No such book found!!");
				} else {
					Optional<Media> disc = storeList.stream().filter(item -> item instanceof DigitalVideoDisc
							&& item.getTitle().toLowerCase().equals(title.toLowerCase())).findFirst();
					if (disc.isPresent()) {
						id++;
						DigitalVideoDisc temp = (DigitalVideoDisc) disc.get();
						temp.setId(id);
						order.addMedia(disc.get());
						System.out.println("Disc " + disc.get().getTitle() +" added");
					} else
						System.out.println("No such disc found!!");
				}

				break;

			case 3:
				if (order == null) {
					System.out.println("No order created");
					break;
				}
				System.out.print("Enter id to delete: ");
				int deleteId = Integer.parseInt(scanner.nextLine());
				
				List<Media> lst = order.getItemsOrdered().stream().filter(item -> item.getId() != deleteId)
						.collect(Collectors.toList());
				
				if (lst.size() == order.getItemsOrdered().size())
					System.out.println("Delete failed! No such id");
				order.setItemsOrdered(lst);
				break;

			case 4:
				if (order == null) {
					System.out.println("No order created");
					break;
				}
				if (order.getItemsOrdered().size() < 5)
					order.printOrder();
				else 
					order.printOrderWithLuckyItem();
				break;

			case 0:
				System.out.println("Exit!");
				break;

			default:
				System.out.println("Invalid choice, try again!");
				break;
			}
		} while (choice != 0);
		scanner.close();
	}
}
