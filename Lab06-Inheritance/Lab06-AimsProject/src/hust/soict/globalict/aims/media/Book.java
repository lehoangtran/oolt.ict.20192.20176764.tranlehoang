package hust.soict.globalict.aims.media;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class Book extends Media {

	private List<String> authors = new ArrayList<>();

	// -------------------- getter setter constructor --------------

	public Book() {
	}

	public Book(String title, String category) {
		super(title, category);
	}

	public Book(String title, String category, List<String> authors) {
		super(title, category);
		this.authors = authors;
		// TODO: check author condition
	}

	public Book(String title, String category, List<String> authors, float cost) {
		super(title, category, cost);
		this.authors = authors;
		// TODO: check author condition
	}

	public Book(int id, String title, String category, float cost, List<String> authors) {
		super(id, title, category, cost);
		this.authors = authors;
		// TODO: check author condition
	}

	public List<String> getAuthors() {
		return authors;
	}

	public void setAuthors(List<String> authors) {
		this.authors = authors;
	}

	// --------------------- service ---------------------------------

	@Override
	public String toString() {
		return "Book [title= " + getTitle() + ", cost = $" + getCost() + "]";
	}

	public void addAuthor(String authorName) {
		if (!authors.contains(authorName)) {
			List<String> res = this.getAuthors();
			res.add(authorName);
			this.setAuthors(res);
		} else
			System.out.println("Author's name already in list");
	}

	public void removeAuthor(String authorName) {
		if (authors.contains(authorName))
			this.setAuthors(
					this.getAuthors().stream().filter(name -> !name.equals(authorName)).collect(Collectors.toList()));
		else
			System.out.println("Author's name is not in list");
	}

}
