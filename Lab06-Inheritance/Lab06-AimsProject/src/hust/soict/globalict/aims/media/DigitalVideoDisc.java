package hust.soict.globalict.aims.media;

public class DigitalVideoDisc extends Media{
	private String directory;
	private int length;
	
	// ----------------- constructor ----------------------
	
	public DigitalVideoDisc() {
	}

	public DigitalVideoDisc(String title) {
		super(title);
	}

	public DigitalVideoDisc(String title, String category) {
		super(title, category);
	}

	public DigitalVideoDisc(String title, String category, String directory) {
		super(title, category);
		this.directory = directory;
	}

	public DigitalVideoDisc(String title, String category, String directory, int length, float cost) {
		super(title, category, cost);
		this.directory = directory;
		this.length = length;
	}
	
	public DigitalVideoDisc(int id, String title, String category, String directory, int length, float cost) {
		super(id, title, category, cost);
		this.directory = directory;
		this.length = length;
	}
	
	// ----------------- getter setter ----------------------

	public String getDirectory() {
		return directory;
	}

	public void setDirectory(String directory) {
		this.directory = directory;
	}

	public int getLength() {
		return length;
	}

	public void setLength(int length) {
		this.length = length;
	}
	
	// ----------------- service ----------------------

	@Override
	public String toString() {
		return "DigitalVideoDisc [title = " + getTitle() + ", cost = $" + getCost() + "]";
	}
	
	// search if a title contains tokens
	public boolean search(String title) {
		title = title.toLowerCase();
		String[] tokens = title.split(" ");
		String media = getTitle().toLowerCase();

		for (int i = 0; i < tokens.length; i++) {
			if (media.indexOf(tokens[i]) == -1)
				return false;
		}
		return true;
	}

}
