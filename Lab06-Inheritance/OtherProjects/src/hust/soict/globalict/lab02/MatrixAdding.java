package hust.soict.globalict.lab02;
import java.util.Arrays;
import java.util.Scanner;

public class MatrixAdding {
	public static void main(String[] args) {
		Scanner scan = new Scanner(System.in);
		
		System.out.print("# rows: ");
		int rows = Integer.parseInt(scan.nextLine());
		System.out.print("# columns: ");
		int cols = Integer.parseInt(scan.nextLine());
		
		int[][] mata = new int[rows][cols];
		int[][] matb = new int[rows][cols];
		int[][] matc = new int[rows][cols];
		
		System.out.println("--- Matrix A ---");
		for(int i = 0; i < rows; i++) {
			for (int j = 0; j < cols; j++) {
				System.out.print("Element["+ (i+1) +"]" + "[" + (j+1)+ "]" +": ");
				mata[i][j] = Integer.parseInt(scan.nextLine());
			}
		}
		
		System.out.println("--- Matrix B ---");
		for(int i = 0; i < rows; i++) {
			for (int j = 0; j < cols; j++) {
				System.out.print("Element["+ (i+1) +"]" + "[" + (j+1)+ "]" +": ");
				matb[i][j] = Integer.parseInt(scan.nextLine());
				matc[i][j] = mata[i][j] + matb[i][j];
			}
		}
		
		System.out.println("Mat A: " + Arrays.deepToString(mata));
		System.out.println("Mat B: " + Arrays.deepToString(matb));
		System.out.println("Sum: " + Arrays.deepToString(matc));
		
		
		
	}
}
