package hust.soict.globalict.lab02;
import java.util.Arrays;
import java.util.Scanner;

public class SortArray {
	public static void main(String[] args) {
		Scanner scan = new Scanner(System.in);
		System.out.print("Enter n: ");
		int n = Integer.parseInt(scan.nextLine());
		int[] arr = new int[n];
		int sum = 0;
		
		for (int i = 0; i < n; i++) {
			System.out.print("Enter element " + (i+1) + ": ");
			arr[i] = Integer.parseInt(scan.nextLine());
			sum += arr[i];
		}
		Arrays.sort(arr);
		System.out.println(Arrays.toString(arr));
		System.out.println("Average value: " + (float)sum/n);
	}
}
