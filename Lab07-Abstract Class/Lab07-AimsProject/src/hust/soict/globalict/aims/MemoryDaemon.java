package hust.soict.globalict.aims;

public class MemoryDaemon implements Runnable{
	private long memoryUsed;
	
	@Override
	public void run() {
		Runtime rt = Runtime.getRuntime();
		long used;
		
		while(true) {
			used = rt.totalMemory() - rt.freeMemory();
			if (used != memoryUsed) {
				System.out.println("Memory Used = " + used);
				memoryUsed = used;
			}
		}
	}
}
