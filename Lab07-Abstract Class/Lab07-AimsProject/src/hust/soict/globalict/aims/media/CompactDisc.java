package hust.soict.globalict.aims.media;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class CompactDisc extends Disc implements Playable {
	private String artist;
	private int length;
	private List<Track> tracks = new ArrayList<>();

	// --------------------- constructor --------------------

	public CompactDisc() {
	}

	public CompactDisc(String title) {
		super(title);
	}

	public CompactDisc(String title, String category, String artist, List<Track> tracks) {
		super(title, category);
		this.artist = artist;
		this.length = getLength();
		this.tracks = tracks;
	}

	public CompactDisc(String title, String category, String director, String artist, List<Track> tracks) {
		super(title, category, director);
		this.artist = artist;
		this.length = getLength();
		this.tracks = tracks;
	}

	public CompactDisc(String title, String category) {
		super(title, category);
	}

	public CompactDisc(String title, String category, String director) {
		super(title, category, director);
	}

	public CompactDisc(String title, String category, String director, float cost) {
		super(title, category, director, cost);
		this.length = getLength();
	}

	public CompactDisc(int id, String title, String category, String director, float cost) {
		super(id, title, category, director, cost);
		this.length = getLength();
	}
	
	public CompactDisc(int id, String title, String artist, List<Track> tracks, float cost) {
		super(id, title, cost);
		this.artist = artist;
		this.length = getLength();
		this.tracks = tracks;
	}

	// ----------------------- getter ----------------------------

	public String getArtist() {
		return artist;
	}

	// ----------------------- service ---------------------------

	@Override
	public String toString() {
		return "CompactDisc [ title=" + getTitle() + "artist=" + artist + ", length=" + length + ", tracks=" + tracks
				+ ", cost = $" + getCost() + "]";
	}

	public void addTrack(Track track) {
		if (!this.tracks.contains(track))
			this.tracks.add(track);
		else
			System.out.println("This track is already existed");
	}

	public void removeAuthor(Track track) {
		if (this.tracks.contains(track))
			this.tracks = this.tracks.stream().filter(t -> t != track).collect(Collectors.toList());
		else
			System.out.println("Track not in list! Cant remove");
	}

	@Override
	public int getLength() {
		return this.tracks.stream().mapToInt(Track::getLength).sum();
	}

	@Override
	public void play() {
		// TODO Auto-generated method stub
		tracks.forEach(track -> {
			track.play();
			System.out.println();
		});

	}

}
