package hust.soict.globalict.aims.media;

public class Disc extends Media implements Playable{
	private int length;
	private String director;

	// ----------------- constructor ----------------------

	public Disc() {
	}
	
	public Disc(int id) {
		super(id);
	}

	public Disc(String title) {
		super(title);
	}

	public Disc(String title, String category) {
		super(title, category);
	}

	public Disc(String title, String category, String director) {
		super(title, category);
		this.director = director;
	}
	
	public Disc(int id, String title, float cost) {
		super(id, title, cost);
	}
	
	public Disc(String title, String category, String director, float cost) {
		super(title, category, cost);
		this.director = director;
	}

	public Disc(String title, String category, String director, int length, float cost) {
		super(title, category, cost);
		this.director = director;
		this.length = length;
	}
	
	public Disc(int id, String title, String category, String director, float cost) {
		super(id, title, category, cost);
		this.director = director;
	}

	public Disc(int id, String title, String category, String director, int length, float cost) {
		super(id, title, category, cost);
		this.director = director;
		this.length = length;
	}
	
	// ----------------- getter setter ----------------------

	public int getLength() {
		return length;
	}

	public String getDirector() {
		return director;
	}

	@Override
	public void play() {
		
	}

}
