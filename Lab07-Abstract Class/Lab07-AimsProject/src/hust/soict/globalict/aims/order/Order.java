package hust.soict.globalict.aims.order;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

import hust.soict.globalict.aims.media.Book;
import hust.soict.globalict.aims.media.CompactDisc;
import hust.soict.globalict.aims.media.DigitalVideoDisc;
import hust.soict.globalict.aims.media.Media;

public class Order {
	public static final int MAX_NUMBER_ORDERED = 10;
	public static final int MAX_LIMITED_ORDERS = 3;
	private static int nbOrders = 0;

	private List<Media> itemsOrdered = new ArrayList<>();
	private Date dateOrdered;

	// ---------getter setter constructor-------------------------

	public Order() {
		if (nbOrders + 1 <= MAX_LIMITED_ORDERS) {
			this.dateOrdered = new Date();
			nbOrders++;
			System.out.println("Order " + nbOrders + " created");
		} else {
			System.out.println("Max orders reached");
		}
	}

	public Date getDateOrdered() {
		return dateOrdered;
	}

	public void setDateOrdered(Date dateOrdered) {
		this.dateOrdered = dateOrdered;
	}

	public List<Media> getItemsOrdered() {
		return itemsOrdered;
	}

	public void setItemsOrdered(List<Media> itemsOrdered) {
		this.itemsOrdered = itemsOrdered;
	}

	// ----------service------------------------

	public void addMedia(Media media) {
		if (itemsOrdered.size() < MAX_NUMBER_ORDERED) {
			itemsOrdered.add(media);
		} else {
			System.out.println("The order is full");
		}
	}

	// Overloading (add a list)
	public void addMedia(List<Media> medias) {
		if (itemsOrdered.size() + medias.size() < MAX_NUMBER_ORDERED) {
			itemsOrdered.addAll(medias);
		} else {
			System.out.println("The order is full...");
		}
	}

	public void removeMedia(Media media) {
		if (itemsOrdered.contains(media)) {
			if (itemsOrdered.size() != 0) {
				itemsOrdered = itemsOrdered.stream().filter(m -> m != media).collect(Collectors.toList());
				System.out.format("The media \"%s\" has been removed\n", media.getTitle());
			} else {
				System.out.println("Order is empty. Please add!");
			}
		} else {
			System.out.println("Your order doesnt contain " + media.getTitle());
		}

	}

	public double totalCost() {
		return itemsOrdered.stream().mapToDouble(m -> m.getCost()).sum();
	}

	public Media getALuckyItem() {
		int randomIndex = (int) (Math.random() * itemsOrdered.size());
//		Media luckyItem = itemsOrdered.get(randomIndex);
//		luckyItem.setCost(0);
		return itemsOrdered.get(randomIndex);
	}

	// --------------------------------------------------

	public void printOrder() {
		System.out.println("************************* ORDER ************************");
		System.out.println("Date: " + getDateOrdered());
		System.out.println("Ordered items: ");

		itemsOrdered.forEach(item -> {
			if (item instanceof DigitalVideoDisc)
				System.out.println(item.getId() + ". " + item.getTitle() + " - " + item.getCategory() + " - "
						+ ((DigitalVideoDisc) item).getDirectory() + " - " + ((DigitalVideoDisc) item).getLength()
						+ ": $" + item.getCost());
			
			if (item instanceof CompactDisc)
				System.out.println(item.getId() + ". " + item.getTitle() + " - " + ((CompactDisc) item).getArtist()
						+ " - " + ((CompactDisc) item).getLength() + ": $" + item.getCost());

			if (item instanceof Book)
				System.out.println(item.getId() + ". " + item.getTitle() + " - " + item.getCategory() + " - "
						+ ((Book) item).getAuthors() + ": $" + item.getCost());
		});

		System.out.format("\nTotal Cost is: %.2f$\n", totalCost());
		System.out.println("******************************************************\n");
	}

	public void printOrderWithLuckyItem() {
		System.out.println("************************* ORDER WITH LUCKY ITEM ************************");
		System.out.println("Date: " + getDateOrdered());
		System.out.println("Ordered items: ");

		itemsOrdered.forEach(item -> {
			if (item instanceof DigitalVideoDisc)
				System.out.println(item.getId() + ". " + item.getTitle() + " - " + item.getCategory() + " - "
						+ ((DigitalVideoDisc) item).getDirectory() + " - " + ((DigitalVideoDisc) item).getLength()
						+ ": $" + item.getCost());

			if (item instanceof CompactDisc)
				System.out.println(item.getId() + ". " + item.getTitle() + " - " + ((CompactDisc) item).getArtist()
						+ " - " + ((CompactDisc) item).getLength() + ": $" + item.getCost());

			if (item instanceof Book)
				System.out.println(item.getId() + ". " + item.getTitle() + " - " + item.getCategory() + " - "
						+ ((Book) item).getAuthors() + ": $" + item.getCost());

		});

		Media luckyItem = getALuckyItem();
		System.out.println("\nLucky Item: " + luckyItem.getTitle() + " - Price: $0");

		System.out.format("\nTotal Cost is: %.2f$\n", totalCost() - luckyItem.getCost());
		System.out.println("******************************************************\n");
	}

}
