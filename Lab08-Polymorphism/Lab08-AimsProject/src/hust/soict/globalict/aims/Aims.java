package hust.soict.globalict.aims;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.Scanner;
import java.util.stream.Collectors;

import hust.soict.globalict.aims.media.Book;
import hust.soict.globalict.aims.media.CompactDisc;
import hust.soict.globalict.aims.media.DigitalVideoDisc;
import hust.soict.globalict.aims.media.Disc;
import hust.soict.globalict.aims.media.Media;
import hust.soict.globalict.aims.media.Track;
import hust.soict.globalict.aims.order.Order;

public class Aims extends Thread {
	private static List<Media> storeList = new ArrayList<>();
	private static int id = 0;

	public static void showItems() {
		storeList.add(new DigitalVideoDisc("The Lion King", "Animation", "Roger Allers", 86, 29.95f));
		storeList.add(new DigitalVideoDisc("Star Wars", "Science Fiction", "George Lucas", 124, 21.95f));
		storeList.add(new DigitalVideoDisc("Alladin", "Animation", "John Musker", 90, 98.99f));
		storeList.add(new DigitalVideoDisc("Harry but not Potter", "Magic", "Hello World", 150, 49.99f));
		storeList.add(new Book("The Lion King 2", "Animation", Arrays.asList("Author a", "Author b"), 51.5f));
		storeList.add(new Book("Star Wars 2", "Science Fiction", Arrays.asList("Author c", "Author b"), 39.5f));
		storeList.add(new Book("Alladin 2", "Animation", Arrays.asList("Author d", "Author p"), 23.0f));
		storeList.add(new Book("Harry Potter", "Magic", Arrays.asList("Author e", "Author s"), 18.5f));
		storeList.add(new Book("The Lion King 3", "Animation", Arrays.asList("Author f", "Author z"), 102.2f));
		storeList.forEach(System.out::println);
	}

	// -------------------- service --------------------------------

	public static void addToOrder2(Order order) {
		Scanner scanner = new Scanner(System.in);
		int choice;
		boolean check;
		System.out.println("You want to order Book, Digital Video Disc or Compact Disc? ");
		System.out.print("1. Book\n2. Digital Video Disc\n3. Compact Disc\nYour choice: ");
		do {
			choice = Integer.parseInt(scanner.nextLine());
			check = 0 <= choice && choice <= 3;
			if (!check)
				System.out.println("Not valid! Enter again");
		} while (!check);

		if (choice == 1) // if user chose to order book
			addBook(order);
		if (choice == 2)
			addDigitalDisc(order);
		if (choice == 3)
			addCompactDisc(order);
	}

	private static void addBook(Order order) {
		Scanner scanner = new Scanner(System.in);
		System.out.print("Enter title: ");
		String title = scanner.nextLine();

		// Find book with ordered title
		Optional<Media> book = storeList.stream()
				.filter(item -> item instanceof Book && item.getTitle().toLowerCase().equals(title.toLowerCase()))
				.findFirst();

		if (book.isPresent()) {
			id++;
			Book temp = (Book) book.get();
			temp = new Book(id, temp.getTitle(), temp.getCategory(), temp.getCost(), temp.getAuthors());
			int prevSize = order.getItemsOrdered().size();
			order.addMedia(temp);
			if (prevSize == order.getItemsOrdered().size())
				id--;
		} else
			System.out.println("No such book found!!");
	}

	private static void addDigitalDisc(Order order) {
		Scanner scanner = new Scanner(System.in);
		System.out.print("Enter title: ");
		String title = scanner.nextLine();

		// Find disc with ordered title
		Optional<Media> disc = storeList.stream().filter(
				item -> item instanceof DigitalVideoDisc && item.getTitle().toLowerCase().equals(title.toLowerCase()))
				.findFirst();

		if (disc.isPresent()) {
			id++;
			DigitalVideoDisc temp = (DigitalVideoDisc) disc.get();
			temp = new DigitalVideoDisc(id, temp.getTitle(), temp.getCategory(), temp.getDirector(), temp.getLength(),
					temp.getCost());
			int prevSize = order.getItemsOrdered().size();
			order.addMedia(temp);

			// ask user play disc option
			if (prevSize != order.getItemsOrdered().size())
				playDiscOption(temp);
			else
				id--;
		} else
			System.out.println("No such disc found!!");
	}

	private static void addCompactDisc(Order order) {
		Scanner scanner = new Scanner(System.in);

		id++;
		System.out.print("Enter disc title: ");
		String title = scanner.nextLine();

		System.out.print("Enter artist: ");
		String artist = scanner.nextLine();

		System.out.print("Enter number of track (each track is $5.9): ");
		int n = Integer.parseInt(scanner.nextLine()); // number of track

		CompactDisc disc = new CompactDisc(id, title, artist, 5.9f * n);

		System.out.println("Enter tracks' infomation: ");
		for (int i = 0; i < n; i++) {
			System.out.println("- Track " + (i + 1));
			System.out.print("Enter track's title: ");
			String trackTitle = scanner.nextLine();

			System.out.print("Enter track's length: ");
			int length = Integer.parseInt(scanner.nextLine());
			disc.addTrack(new Track(trackTitle, length));
		}

		int prevSize = order.getItemsOrdered().size();
		order.addMedia(disc);
		if (prevSize != order.getItemsOrdered().size())
			if (disc.getLength() > 0)
				playDiscOption(disc);
			else
				System.out.println("No track found");
		else
			id--;

	}

	private static void playDiscOption(Disc temp) {
		Scanner scanner = new Scanner(System.in);
		// TODO Auto-generated method stub
		System.out.println("Do you want to play the selected disc?");
		String choice;
		boolean check;
		do {
			System.out.print("Yes/No -> ");
			choice = scanner.nextLine();
			check = choice.toLowerCase().equals("yes") || choice.toLowerCase().equals("no");
			if (!check)
				System.out.println("Not valid!");
		} while (!check);

		if (choice.toLowerCase().equals("yes")) {
			System.out.println("------");
			temp.play();
		}
	}

	// ------------------------- MAIN ---------------------------------------

	public static void showMenu() {

		System.out.println("------------ All items in stock --------------");
		showItems();
		System.out.println("\nOrder Management Application: ");
		System.out.println("--------------------------------");
		System.out.println("1. Create new order (Max: 3 orders)");
		System.out.println("2. Add item to the order (Max: 10 items)");
		System.out.println("3. Delete item by id");
		System.out.println("4. Display the items list of order");
		System.out.println("0. Exit");
		System.out.println("--------------------------------");
		System.out.println("Please choose a number: 0-1-2-3-4");
		System.out.println("**HOT** Buy 5 get 1 random item for free of charge");
	}

	public static void main(String[] args) {
//		Thread thread = new Thread(new MemoryDaemon());
//		thread.setDaemon(true);
//		thread.start();

		int choice;
		Order order = null;
		Scanner scanner = new Scanner(System.in);

		showMenu();
		do {
			System.out.println("\n--------------------------------");
			System.out.print("Input a choice: ");
			
			choice = Integer.parseInt(scanner.nextLine());

			switch (choice) {
			case 1:
				order = new Order();
				id = 0;
				break;

			case 2:
				if (order == null) {
					System.out.println("No order created");
					break;
				}
				addToOrder2(order);

				break;

			case 3:
				if (order == null) {
					System.out.println("No order created");
					break;
				}
				System.out.print("Enter id to delete: ");
				try {
					int deleteId = Integer.parseInt(scanner.nextLine());
					List<Media> lst = order.getItemsOrdered().stream().filter(item -> item.getId() != deleteId)
							.collect(Collectors.toList());

					if (lst.size() == order.getItemsOrdered().size())
						System.out.println("Delete failed! No such id");
					else
						System.out.println("item deleted");
					order.setItemsOrdered(lst);
				} catch (RuntimeException re) {
					System.out.println("error id");
					System.out.println(re.toString());
				}
				break;

			case 4:
				if (order == null) {
					System.out.println("No order created");
					break;
				}
				if (order.getItemsOrdered().size() < 5)
					order.printOrder();
				else
					order.printOrderWithLuckyItem();
				break;

			case 0:
				System.out.println("Exit!");
				break;

			default:
				System.out.println("Invalid choice, try again!");
				break;
			}
		} while (choice != 0);
		scanner.close();

	}
}
