package hust.soict.globalict.aims.media;

public abstract class Media implements Comparable {
	private int id;
	private String title;
	private String category;
	private float cost;

	// ----------------- constructor ----------------------

	public Media() {

	}

	public Media(int id) {
		this.id = id;
	}

	public Media(String title) {
		this.title = title;
	}

	public Media(String title, float cost) {
		this.title = title;
		this.cost = cost;
	}

	public Media(int id, String title) {
		this.id = id;
		this.title = title;
	}

	public Media(String title, String category) {
		this.title = title;
		this.category = category;
	}

	public Media(int id, String title, float cost) {
		this.id = id;
		this.title = title;
		this.cost = cost;
	}

	public Media(String title, String category, float cost) {
		this.title = title;
		this.category = category;
		this.cost = cost;
	}

	public Media(int id, String title, String category, float cost) {
		this.id = id;
		this.title = title;
		this.category = category;
		this.cost = cost;
	}

	// ----------------- getter setter ----------------------

	public int getId() {
		return id;
	}

	public String getTitle() {
		return title;
	}

	public String getCategory() {
		return category;
	}

	public float getCost() {
		return cost;
	}

	@Override
	public boolean equals(Object o) {
		if (o == this) {
			return true;
		}
		if (!(o instanceof Media)) {
			return false;
		}
		Media c = (Media) o;
		return getTitle().equals(c.getTitle());
	}

//	@Override
//	public int compareTo(Object o) {
//		Media c = (Media) o;
//		return getTitle().compareTo(c.getTitle());
//	}
}
