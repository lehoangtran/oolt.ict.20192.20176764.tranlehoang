package hust.soict.globalict.aims.media;

public class Track implements Playable, Comparable{
	private String title;
	private int length;

	public String getTitle() {
		return title;
	}

	public int getLength() {
		return length;
	}
	
	public Track() {
		
	}
	
	public Track(String title, int length) {
		this.title = title;
		this.length = length;
	}

	@Override
	public void play() {
		// TODO Auto-generated method stub
		System.out.println("Playing DVD: " + this.getTitle());
		System.out.println("DVD length: " + this.getLength());
	}
	
	@Override
	public boolean equals(Object o) {
		if (o == this) { 
            return true; 
        } 
        if (!(o instanceof Track)) { 
            return false; 
        } 
        Track c = (Track) o; 
        return getLength() == c.getLength() && getTitle().equals(c.getTitle());
	}
	
	@Override
	public int compareTo(Object o) {
		Track t = (Track) o;
		return getTitle().compareTo(t.getTitle());
	}
	

}
