package hust.soict.globalict.test.media;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import hust.soict.globalict.aims.media.CompactDisc;
import hust.soict.globalict.aims.media.DigitalVideoDisc;
import hust.soict.globalict.aims.media.Media;

public class TestMediaCompareTo {
	public static void main(String[] args) {
		List<CompactDisc> discs = new ArrayList<>();
		List<DigitalVideoDisc> dvds = new ArrayList<>();

		discs.add(new CompactDisc("CD1"));
		discs.add(new CompactDisc("VCD2"));
		discs.add(new CompactDisc("ACD2"));
		dvds.add(new DigitalVideoDisc("The Lion King", 87, 15.2f));
		dvds.add(new DigitalVideoDisc("Ftar Wars", 124, 20.2f));
		dvds.add(new DigitalVideoDisc("Alladin", 90, 10.5f));

		System.out.println("The DVDs currently in the order are:");
		discs.forEach(i -> System.out.println(((Media) i).getTitle() + "- $" + ((Media) i).getCost()));
		dvds.forEach(i -> System.out.println(((Media) i).getTitle() + "- $" + ((Media) i).getCost()));
		
		Collections.sort(discs);
		Collections.sort(dvds);
		System.out.println("----------------\nThe DVDs in sorted order are: ");
		discs.forEach(i -> System.out.println(((Media) i).getTitle() + "- $" + ((Media) i).getCost()));
		dvds.forEach(i -> System.out.println(((Media) i).getTitle() + "- $" + ((Media) i).getCost()));


	}
}
