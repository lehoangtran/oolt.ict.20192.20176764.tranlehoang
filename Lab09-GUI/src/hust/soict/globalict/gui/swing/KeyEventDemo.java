package hust.soict.globalict.gui.swing;

import java.awt.Container;
import java.awt.FlowLayout;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.SwingUtilities;

public class KeyEventDemo extends JFrame implements KeyListener {

	private JTextField tfInput; // Single-line TextField to receive tfInput key
	private JTextArea taDisplay; // Multi-line TextArea to taDisplay result

	public KeyEventDemo() {
		Container cp = getContentPane();
		cp.setLayout(new FlowLayout()); // The content-pane sets its layout

		cp.add(new JLabel("Enter Text: "));

		tfInput = new JTextField(10);
		cp.add(tfInput);

		taDisplay = new JTextArea(5, 40);
		cp.add(taDisplay);

		tfInput.addKeyListener(this);

		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE); // Exit program if close-window button clicked

		setTitle("Key Event Demo"); // "super" JFrame sets title
		setSize(400, 100); // "super" JFrame sets initial size
		setVisible(true); // "super" JFrame shows
	}

	public static void main(String[] args) {
		SwingUtilities.invokeLater(new Runnable() {
			@Override
			public void run() {
				new KeyEventDemo(); // Let the constructor do the job
			}
		});
	}

	@Override
	public void keyTyped(KeyEvent evt) {
		taDisplay.append("You have typed " + evt.getKeyChar() + "\n");

	}

	@Override
	public void keyPressed(KeyEvent e) {
		// TODO Auto-generated method stub

	}

	@Override
	public void keyReleased(KeyEvent e) {
		// TODO Auto-generated method stub

	}

}
