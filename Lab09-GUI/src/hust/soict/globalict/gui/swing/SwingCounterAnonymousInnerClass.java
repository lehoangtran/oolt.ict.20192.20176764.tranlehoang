package hust.soict.globalict.gui.swing;

import java.awt.Container;
import java.awt.FlowLayout;
import java.awt.Label;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JTextField;
import javax.swing.SwingUtilities;

public class SwingCounterAnonymousInnerClass extends JFrame {
	private JTextField tfCount;
	private JButton btnCount;
	private int count = 0;

	public SwingCounterAnonymousInnerClass() {
		Container cp = getContentPane();
		cp.setLayout(new FlowLayout());

		cp.add(new Label("Counter")); // An anonymous instance of Label

		tfCount = new JTextField("0", 10);
		tfCount.setEditable(false); // read-only
		cp.add(tfCount); // "super" Frame adds tfCount

		btnCount = new JButton("Count");
		cp.add(btnCount); // "super" Frame adds btnCount

		// Construct an anonymous instance of an anonymous class.
		// btnCount adds this instance as a ActionListener.
		btnCount.addActionListener(evt -> tfCount.setText(++count + ""));

		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

		setTitle("AWT Counter");
		setSize(300, 150);
		setVisible(true);
	}

	// The entry main method
	public static void main(String[] args) {
		SwingUtilities.invokeLater(new Runnable() {
			@Override
			public void run() {
				new SwingCounterAnonymousInnerClass(); // Let the constructor do the job
			}
		});
	}
}
