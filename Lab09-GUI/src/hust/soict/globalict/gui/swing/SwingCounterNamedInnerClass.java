package hust.soict.globalict.gui.swing;

import java.awt.Container;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JTextField;
import javax.swing.SwingUtilities;

public class SwingCounterNamedInnerClass extends JFrame {
	private JTextField tfCount;
	private JButton btnCount;
	private int count = 0;

	// Constructor to setup the GUI components and event handlers
	public SwingCounterNamedInnerClass() {
		// Retrieve the top-level content-pane from JFrame
		Container cp = getContentPane();

		// Content-pane sets layout
		cp.setLayout(new FlowLayout());

		cp.add(new JLabel("Counter"));

		tfCount = new JTextField("0", 10);
		tfCount.setEditable(false); // read-only
		cp.add(tfCount);

		btnCount = new JButton("Count");
		cp.add(btnCount);

		btnCount.addActionListener(evt -> tfCount.setText(++count + ""));

		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

		setTitle("Swing Counter"); // "super" JFrame sets title
		setSize(300, 150); // "super" JFrame sets initial size
		setVisible(true); // "super" JFrame shows
	}

	public static void main(String[] args) {
		SwingUtilities.invokeLater(new Runnable() {
			@Override
			public void run() {
				new SwingCounterNamedInnerClass(); // Let the constructor do the job
			}
		});
	}
}
