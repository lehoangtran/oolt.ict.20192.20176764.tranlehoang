package hust.soict.globalict.students;

public class IllegalDobException extends Exception {

	public IllegalDobException() {
		// TODO Auto-generated constructor stub
	}

	public IllegalDobException(String message) {
		super(message);
		// TODO Auto-generated constructor stub
	}

	public IllegalDobException(Throwable cause) {
		super(cause);
		// TODO Auto-generated constructor stub
	}

	public IllegalDobException(String message, Throwable cause) {
		super(message, cause);
		// TODO Auto-generated constructor stub
	}

	public IllegalDobException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
		// TODO Auto-generated constructor stub
	}

}
